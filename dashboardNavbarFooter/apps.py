from django.apps import AppConfig


class DashboardnavbarfooterConfig(AppConfig):
    name = 'dashboardNavbarFooter'

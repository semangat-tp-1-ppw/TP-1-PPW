from django.shortcuts import render
from django.http import HttpResponseRedirect
from status_page.models import Status
from add_friend.models import Friends
from profile_page.views import nama_profile

author = 'Fadhlan Hazmi, Indra Septama, Jovanta Anugerah, Naomi Riana'
def index(request):
	response = {'author': author}
	response['contentss']=''
	response['created_date']=''
	if (Status.objects.all().count() > 0):
		feed = Status.objects.all().order_by('-created_date')[0]
		response['contentss'] = feed.status_content
		response['created_date'] = feed.created_date
	response['friends'] = Friends.objects.all().count()
	response['feeds'] = Status.objects.all().count()
	response['nama'] = nama_profile
	html = 'dashboard.html'
	return render(request, html, response)

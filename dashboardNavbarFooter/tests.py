from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from status_page.models import Status

class DashboardUnitTest(TestCase):
	def test_dashboard_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)

	def test_dashboard_contains_navbar_and_copyright(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')

		self.assertIn('<nav',html_response)
		self.assertIn('&copy;',html_response)

	def test_dashboard_contains_feed(self):
		test = 'Test'
		response_post = Client().post('/status-page/status_post', {'status_content': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


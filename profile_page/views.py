from django.shortcuts import render
response = {'author': 'Fadhlan Hazmi, Indra Septama, Jovanta Anugerah, Naomi Riana'}
nama_profile= 'alex Jahmer'
birth_profile = '12/02/1998'
gender_profile = 'male'
bio_profile = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
email_profile = 'email@mail.com'
# Create your views here.
def index(request):
    response['name'] = nama_profile
    response['birth'] = birth_profile
    response['gender'] = gender_profile
    response['description'] = bio_profile
    response['email'] = email_profile
    html = 'profile.html'

    return render(request, html,response)

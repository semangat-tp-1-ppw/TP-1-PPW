from django import forms

class Forms_add(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
		'invalid': 'Isi input dengan URL',
		}

	name_attrs = {
		'type':'text',
		'class':'form-control',
		'placeholder' : 'Masukkan nama anda',
	}

	url_attrs = {
		'type':'url',
		'class':'form-control',
		'placeholder' : 'Masukkan url anda',
		}

	name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
	url = forms.URLField(label='URL', required=True, widget=forms.TextInput(attrs=url_attrs))
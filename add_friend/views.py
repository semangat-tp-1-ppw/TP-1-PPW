from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms_add import Forms_add
from .models import Friends
from urllib.request import urlopen
from urllib.error import URLError
from dashboardNavbarFooter.views import author


# Create your views here.
response = {'author':author}
def index(request):
	friend = Friends.objects.all()
	response['friend'] = friend
	html = 'add_friend.html'
	response['forms_add'] = Forms_add
	return render(request, html, response)

def add_friend(request):
	form = Forms_add(request.POST or None)
	if(request.method == 'POST' and form.is_valid() and url_is_valid(request.POST['url'])):
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['url'] = request.POST['url']
		friends = Friends(name=response['name'], url=response['url'])
		friends.save()
		return HttpResponseRedirect('/add_friend/')
	else:        
		return HttpResponseRedirect('/add_friend/')

def url_is_valid(url):
    try:
        thepage = urlopen(url)
    except URLError as e:
        return False
    else:
        return True

from django.conf.urls import url
from .views import index, status_post

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^status_post', status_post, name='status_post'),
]
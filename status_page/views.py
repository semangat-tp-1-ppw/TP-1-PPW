from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import  Status_Form
from .models import  Status

# Create your views here.
response = {}
def index(request):
    response['author'] ='Nao'
    response['status_form'] = Status_Form
    response['all_status'] = Status.objects.all().order_by('created_date').reverse()
    html = 'status_page/status_page.html'
    return  render(request, html, response )

def status_post(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST'):
        response['status_content'] = request.POST['status_content']
        status_jadi = Status(status_content=response['status_content'])
        status_jadi.save()
        return  HttpResponseRedirect('/status-page/')
    else:
        return  HttpResponseRedirect('/status-page/')

from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid' : 'Isi input dengan email',
    }
    attrs = {
        'type' : 'text',
        'class': 'status-form-textarea',
        'placeholder' : 'Update status here',
    }

    status_content = forms.CharField(label='', widget=forms.Textarea(attrs=attrs), required=True)
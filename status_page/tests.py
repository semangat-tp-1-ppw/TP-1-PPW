from django.test import TestCase
from django.test import Client
from django.urls import resolve
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.chrome.options import Options

from .models import  Status
from .views import index
from .forms import Status_Form

# Create your tests here.
class StatusPageUnitTest(TestCase):
    def test_status_page_url_is_exist(self):
        response = Client().get('/status-page/')
        self.assertEqual(response.status_code, 200)

    def test_status_page_using_index_func(self):
        found = resolve('/status-page/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        new_status =Status.objects.create(status_content='ini status')
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status_content' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_content'],
            ["This field is required."]
        )

    def test_status_post_success_and_render_result(self):
        test = 'Test'
        response_post = Client().post('/status-page/status_post', {'status_content': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status-page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_post_fail_and_render_result(self):
        test = 'Test'
        response_post = Client().post('/status-page/status_post', {'status_content': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status-page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_root_url_now_is_using_index_page_from_status_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/status-page/',301,200)

    def test_status_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status-page/status_post', {'status_content': ""})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status-page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status-page/status_post', {'status_content': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status-page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_message_post_using_non_post_method(self):
        response = Client().get('/status-page/status_post')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/status-page/',302,200)




